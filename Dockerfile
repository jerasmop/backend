#Image docker base inicial
FROM node

#Crea y danombre al directorio de trabajo del contenedor docket
WORKDIR /docker-dir-apitechu

#Copiar archivos del proyecto en el directorio de traajo de docker
ADD . /docker-dir-apitechu

#Instalar dependencias de produccion del proyecto
#RUN  npm install --only=produccion

#Puerto donde expondremos nuestro contendor
EXPOSE 3000

#Comando para lanzar app
CMD ["npm","run","prod"]
